import React  from 'react';
import Telegram from "./telegram_clone/telegram";
import {BrowserRouter, Switch, Route} from "react-router-dom";

function App() {
    return (
        <div className="App" style={{width:"100%"}}>
            <BrowserRouter>
                <Switch>
                    <Route path="/"  component={Telegram}/>
                </Switch>
            </BrowserRouter>
        </div>
  );
}

export default App;
