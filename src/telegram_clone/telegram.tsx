import React, {useState} from 'react';
import { Layout, Menu } from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UnorderedListOutlined,
    PartitionOutlined,
} from '@ant-design/icons';
import "./telegram.css";
import {Link, Route, Switch} from "react-router-dom";
import IndexTodos from "../todos/indexTodos";
import IndexPagination from "../pagination/indexPagination";
const { Header, Sider, Content } = Layout;



function Telegram() {
    const [collapsed, setCollapsed]=useState<boolean>(false);
    const toggle = () => {
        setCollapsed(!collapsed);
    };

    return (
        <div className="telegram" style={{width:"100%", height:"100vh"}}>
            <Layout>
                <Sider trigger={null} collapsible collapsed={collapsed}>
                    <div className="logo" />
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                        <Menu.Item key="1" icon={<UnorderedListOutlined />}>
                            <Link to="/todo">
                                TodoList
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="2" icon={<PartitionOutlined />}>
                            <Link to="/pagination">
                                Pagination
                            </Link>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Header className="site-layout-background" style={{ padding: 0 }}>
                        {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                            className: 'trigger',
                            onClick: toggle,
                        })}
                       <div className="logo-text">
                           TypeScript + AntDesign
                       </div>
                    </Header>
                    <Content
                        className="site-layout-background"
                        style={{
                            margin: '24px 16px',
                            padding: 24,
                            minHeight: 280,
                        }}
                    >
                        <Switch>
                            <Route path="/todo" exact component={IndexTodos}/>
                            <Route path="/pagination" exact component={IndexPagination}/>
                        </Switch>
                    </Content>
                </Layout>
            </Layout>
        </div>
    );
}

export default Telegram;