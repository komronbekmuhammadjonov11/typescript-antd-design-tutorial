import React, {useState} from 'react';
import {Col, Row} from "antd";
import "./css/main.css";
const IndexGame=()=> {
    const [numberWon, setNumberWon]=useState(10);
    const [numberTwo, setNumberTwo]=useState(20);
    const [time, setTime]=useState(10);



    const timer=()=>{

    };
    // let timeInterval=setInterval(()=>{
    //     if (time<=0){
    //         clearInterval(timeInterval);
    //     }
    //     setTime((prevState => prevState-1))
    // } , 1000);

    const answer=()=>{

    };
    return (
        <div className="game-content">
            <Row>
                <Col span={8} offset={8}>
                    <div className="time">
                        <h1>{time}</h1>
                    </div>
                    <div className="resurs">
                        <h2 className="number-won">{numberWon}</h2>
                        <h2>+</h2>
                        <h2 className="number-two">{numberTwo}</h2>
                    </div>
                    <div className="answers">
                        <div className="buttons">
                            <button onClick={answer}>13</button>
                            <button onClick={answer}>20</button>
                        </div>
                        <div className="buttons">
                            <button onClick={answer}>60</button>
                            <button onClick={answer}>14</button>
                        </div>
                    </div>
                </Col>
            </Row>
        </div>
    );
}

export default IndexGame;