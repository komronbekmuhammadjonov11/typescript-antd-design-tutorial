import React, {useEffect, useState} from 'react';
import {Alert, Button, Col, Row} from "antd";
import axios from "axios"
interface dataType {
    id:number,
    userId:number,
    title:string,
    body:string
}
function IndexPagination() {
    let data:dataType[]=[];
   const [resultArray, setResultArray]=useState<dataType[]>([]);
    useEffect(  ()=>{
        axios.get('https://jsonplaceholder.typicode.com/posts').then((res)=>{
            data=res.data;
            buttonClick(1);
        });
    }, []);
    let messageCount:number=10;
    let pageNumber:number=5;
    const buttonClick = (index: number ) => {
        axios.get('https://jsonplaceholder.typicode.com/posts').then((res)=>{
            let startCount: number;
            let endCount: number;
            startCount = messageCount * (index - 1);
            endCount = index * messageCount;
            console.log(data);
            setResultArray(res.data.slice(startCount, endCount));
        });
        // 1 bolganda 1 dan 10 gacha
        // 2 bolganda 10 20 gacha
        // 3 bolganda 20 30gacha
    };
    return (
        <div className="index-pagination" style={{paddingTop:"50px"}}>
            <Row>
                {resultArray.map((item) => {
                    return <Col key={item.id} span={12} offset={6}>
                       <div key={item.id}>
                           <Alert
                               key={item.id}
                               message={item.title}
                               type="success"
                               style={{marginBottom:"5px"}}
                           />
                       </div>
                    </Col>
                })}
                <Col span={12} offset={6}>
                    <div style={{display:"flex"}}>
                        <Button type="primary" onClick={()=>buttonClick(1)} style={{marginRight:"10px"}}>1</Button>
                        <Button type="primary" onClick={()=>buttonClick(2)} style={{marginRight:"10px"}}>2</Button>
                        <Button type="primary" onClick={()=>buttonClick(3)} style={{marginRight:"10px"}}>3</Button>
                        <Button type="primary" onClick={()=>buttonClick(4)} style={{marginRight:"10px"}}>4</Button>
                        <Button type="primary" onClick={()=>buttonClick(5)} style={{marginRight:"10px"}}>5</Button>
                    </div>
                </Col>
            </Row>
        </div>
    );
}

export default IndexPagination;