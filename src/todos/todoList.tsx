import React, {Dispatch, SetStateAction, useState} from "react";
import {Button, Checkbox, Col, Input, Modal, Row} from "antd";
import {DeleteOutlined,  EditOutlined} from '@ant-design/icons';
import {todoInterface} from "./interface/interface";
// import {CheckboxChangeEvent} from "antd/es/checkbox";
const style={
    width:"100%",
    padding:"20px",
    backgroundColor:"#40a9ff",
    display:"flex",
    justifyContent:"space-between",
    marginBottom:"5px",
    color:"#fff"
};
interface todoListProps {
    todoList:todoInterface[],
    deleteTodos(id:number):void,
    checkboxChange(checked:boolean, id:number):void,
    editTodos(object:todoInterface):void
}
const TodoList:React.FunctionComponent<todoListProps>=(props)=>{

    const [todos, setTodos]=useState<todoInterface>({
        id:0,
        msg:"",
        is_checked:false
    });

    const [isModalVisible, setIsModalVisible] = useState<boolean>(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = (id:number) => {
        props.editTodos(todos);
        setIsModalVisible(false);
    };


    const handleCancel = () => {
        setIsModalVisible(false);
    };


    const editTodos=(id:number)=>{
      showModal();
      let editTodos:todoInterface | undefined;
      editTodos=props.todoList.find((todos)=>todos.id===id);
      if (typeof editTodos!=="undefined"&&editTodos.id!==0){
          setTodos(editTodos);
      }
    };

    const deleteTodos=(id:number)=>{
        props.deleteTodos(id)
    };

    const handleInputChange=(event:React.ChangeEvent<HTMLInputElement>)=>{
      let newObject:todoInterface={
          ...todos,
          [event.target.name]:event.target.value
      };
      setTodos(newObject);
    };


    // @ts-ignore
    const handleCheckbox =(e:React.CheckboxChangeEvent<HTMLInputElement>, id:number)=>{
        props.checkboxChange(e.target.checked, id);
    };


    return (
        <div className="todoList" style={{marginTop:"20px"}}>
            {props.todoList.map((item)=>{
                return  <div key={item.id}>
                    <div className="message" style={style}>
                        {item.msg}
                        <div className="buttons" style={{display: "flex"}}>
                            <div className="checked-section">
                                <Checkbox
                                    checked={item.is_checked}
                                    onChange={(e)=>handleCheckbox(e, item.id)}
                                >
                                </Checkbox>
                            </div>
                            <div className="edit-icon" onClick={()=>editTodos(item.id)} style={{margin: "0 20px"}}>
                                <EditOutlined/>
                            </div>
                            <div
                                className="delete-icon "
                                onClick={() => deleteTodos(item.id)}
                            >
                                <DeleteOutlined/>
                            </div>
                        </div>
                    </div>
                    <Modal title="Basic Modal" visible={isModalVisible} onOk={()=>handleOk(item.id)} onCancel={handleCancel}>
                        <Input
                            type="text"
                            size="large"
                            defaultValue={todos.msg}
                            onChange={handleInputChange}
                            name="msg"
                        />
                    </Modal>
                </div>
            })}
        </div>
    )
};

export default TodoList;