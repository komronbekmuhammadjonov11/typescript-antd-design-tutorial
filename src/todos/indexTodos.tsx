import React, {useState} from 'react';
import TodosForm from "./todosForm";
import TodoList from "./todoList";
import {Col, Progress, Row} from "antd";
import {todoInterface} from "./interface/interface";


function IndexTodos() {
    const [todoList, setTodoList]=useState<todoInterface[]>([]);
    const addTodoList=(title:string)=>{
        let newObject:todoInterface={
            id:Date.now(),
            msg:title,
            is_checked:false
        };
        setTodoList((prev)=>prev.concat(newObject));
    };
    const deleteTodos=(id:number)=>{
        let newArray:todoInterface[]=[];
        newArray=todoList.filter(item=>item.id!==id);
        setTodoList(newArray);
    };


    const editTodos=(object:todoInterface)=>{
        let newArray:todoInterface[]=[];
        newArray=todoList.map((item)=>{
            if (item.id===object.id){
                return object;
            }else {
                return item;
            }
        });
        setTodoList(newArray);
    };


    const checkboxChange=(checked:boolean, id:number)=>{
        let newArray:todoInterface[]=[];
        newArray=todoList.map((item)=>{
            if (id===item.id){
                if (checked){
                    return {
                        ...item,
                        is_checked:true
                    }
                }else {
                    return {
                        ...item,
                        is_checked:false
                    }
                }
            }else return item;
        });
        setTodoList(newArray);
    };

    let todoListLength:number=todoList.length;
    let todoListActionLength:number=0;
    let percentNumber:number=0;
    const todoListFor=()=>{
        if (todoList.length>1){
            todoList.forEach((item)=>{
                if (item.is_checked){
                    todoListActionLength+=1;
                }
            });
            percentNumber=Math.floor(todoListActionLength/todoListLength*100);
        }
    };
    todoListFor();
    console.log(percentNumber);
    return (
            <div>
                <Row>
                    <Col span={8} offset={2}>
                        <TodosForm addTodoList={addTodoList} />
                        <TodoList
                            todoList={todoList}
                            deleteTodos={deleteTodos}
                            checkboxChange={checkboxChange}
                            editTodos={editTodos}
                        />
                    </Col>
                    <Col span={8} offset={2}>
                        <Progress
                            type="circle"
                            style={{marginTop:"100px"}}
                            percent={percentNumber}
                            format={() => `${percentNumber}%`}
                        />
                    </Col>
                </Row>
            </div>
    );
}

export default IndexTodos;
