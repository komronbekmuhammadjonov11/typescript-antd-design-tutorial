import React, {useState} from "react";
import {Col, Input, Row} from "antd";

interface Interface {
    addTodoList(title:string):void
}

const TodosForm : React.FunctionComponent <Interface>=(props)=>{
    const [todos, setTodos]=useState<string>('');
    const handleInputChange=(event:React.ChangeEvent<HTMLInputElement>)=>{
        setTodos(event.target.value);
    };
    const keyPressInput=(event:React.KeyboardEvent)=>{
       if (event.key==="Enter"){
         props.addTodoList(todos);
         setTodos("");
       }
    };
    return (
        <div className="todosForm" style={{marginTop:"150px"}}>
            <Input
                size="large"
                type="text"
                placeholder="Todos..."
                value={todos}
                onChange={handleInputChange}
                onKeyPress={keyPressInput}
            />
        </div>
    )
};
export default TodosForm;